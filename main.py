def over_80(in_list: list) -> list:
    """
    Выводит список чисел, которые больше 80
    :param in_list: входной список с числами
    :return: out_list: выходной список
    """
    out_list = []
    for num in in_list:
        if num > 80:
            out_list.append(num)
    return out_list


def max_number(in_list: list):
    """
    Возвращает максимальное число в списке
    :param in_list:
    :return:
    """
    max_num = 0
    for num in in_list:
        if num > max_num:
            max_num = num
    return max_num


def is_number(x) -> bool:
    """
    Проверяет, явлеяется ли входной параметр числом (int, float)
    :param x:
    :return:
    """
    if type(x) in (int, float):
        return True
    return False


def sum_degrees(num1, num2):
    """
    Вычисляет сумму квадратов 2 чисел
    :param num1:
    :param num2:
    :return:
    """
    return num1**2 + num2**2


nums = [1, 2, 89, 456, 3, 0, 325, 6436, 4, 3, 1]
print("Входной список ", nums)
print("\n")

print("1) Список чисел больше 80: \n", over_80(nums))
print("\n")
print("2) Максимальное число в списке: ", max_number(nums))
print("\n")
x = 5.43
print("3)", x, "является числом? ", is_number(5.43))
print("\n")
num1 = 6
num2 = 3
print("4) Сумма квадратов: ", num1.__str__() + "^2 +", num2.__str__()+"^2 =", sum_degrees(6, 3))
