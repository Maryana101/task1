# Task1



# Задание 1
Функция, возвращающая числа, которые больше 80
Вход: список чисел
Выход: список чисел

# Задание 2
Функция, определяющая максимальное число в списке
Вход: список чисел
Выход: число

# Задание 3
Функция, определяющая, является ли переданное значение числом
вход: некоторое значение
Выход: bool

# Задание 4
Сумма квадратов 2 чисел
Вход: 2 числа
Выход: число
